package de.whitefallen.kotlinlearning.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotBlank


@Entity
data class Artist (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        @get: NotBlank
        val name: String = "",
        @get: NotBlank
        val url: String = ""
)
package de.whitefallen.kotlinlearning.repository

import de.whitefallen.kotlinlearning.entity.Artist
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ArtistRepository : JpaRepository<Artist, Long>
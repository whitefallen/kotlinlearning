package de.whitefallen.kotlinlearning

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinlearningApplication

fun main(args: Array<String>) {
	runApplication<KotlinlearningApplication>(*args)
}

package de.whitefallen.kotlinlearning.controller

import de.whitefallen.kotlinlearning.entity.Artist
import de.whitefallen.kotlinlearning.repository.ArtistRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class ArtistController(private val artistRepository: ArtistRepository) {

    @GetMapping("/artists")
    fun getAllArtists(): List<Artist> = artistRepository.findAll()

    @PostMapping("/artists")
    fun createNewArtist(@Valid @RequestBody artist: Artist): Artist = artistRepository.save(artist)

    @GetMapping("/artists/{id}")
    fun getArtistById(@PathVariable(value="id") artistId: Long): ResponseEntity<Artist> {
        return artistRepository.findById(artistId).map {
            artist -> ResponseEntity.ok(artist)
        }.orElse(ResponseEntity.notFound().build())
    }

    @PutMapping("/artists/{id}")
    fun updateArtistById(@PathVariable(value="id") artistId: Long, @Valid @RequestBody newArtist: Artist): ResponseEntity<Artist> {
        return artistRepository.findById(artistId).map {
            existingArtist -> val updatedArtist: Artist = existingArtist.copy(name = newArtist.name, url = newArtist.url)
            ResponseEntity.ok().body(artistRepository.save(updatedArtist))
        }.orElse(ResponseEntity.notFound().build())
    }

    @DeleteMapping("/artists/{id}")
    fun deleteArtistById(@PathVariable(value = "id") artistId: Long): ResponseEntity<Void> {
        return artistRepository.findById(artistId).map { artist ->
            artistRepository.delete(artist)
            ResponseEntity<Void>(HttpStatus.OK)
        }.orElse(ResponseEntity.notFound().build())
    }
}